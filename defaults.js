function defaults(userObject, defaultProps) {
    if (typeof (userObject) === 'object' && typeof (defaultProps) === 'object') {

        for (let defaultPropsKey in defaultProps) {
            if (userObject[defaultPropsKey] === undefined) {
                userObject[defaultPropsKey] = defaultProps[defaultPropsKey]
            }
        }
        return userObject

    } else {
        console.log("your passed agrument is not object, please try again")
        return {}
    }
}

module.exports = defaults