function pairs(userObject) {

    if (typeof (userObject) === 'object') {

        let pairsList = []

        for (let key in userObject) {
            pairsList.push([key, userObject[key]])
        }
        return pairsList

    } else {
        console.log("your passed agrument is not object, please try again")
        return []
    }
}

module.exports = pairs