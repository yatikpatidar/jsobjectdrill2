function keys(userObject) {
    if (typeof (userObject) === 'object') {

        let keysList = []
        for (let key in userObject) {
            keysList.push(key)
        }
        return keysList

    } else {
        console.log("your passed agrument is not object, please try again")
        return []
    }
}

module.exports = keys