function invert(userObject) {

    if (typeof (userObject) === 'object') {

        let result = {}
        for (let key in userObject) {
            result[userObject[key]] = key
        }
        return result

    } else {
        console.log("your passed agrument is not object, please try again")
        return {}
    }
}

module.exports = invert