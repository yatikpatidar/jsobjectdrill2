const mapOfObject = require('../mapOfObject')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function callback(key, val) {

    if(typeof(val) === 'number'){
        return val + 10
    }else{
        return val.toUpperCase()
    }
}

let result = mapOfObject(testObject, callback)

if (result.length != 0) {
    console.log("mapping of object for our object is :- ", result)
}