const defaults = require('../defaults')


const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

let defaultProps = { position: "Javascript Developer", age: 32}
let result = defaults(testObject, defaultProps)

if (result.length != 0) {
    console.log("default prop of our object is :- ", result)
}