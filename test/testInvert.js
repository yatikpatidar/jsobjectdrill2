const invert = require('../invert')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

let result = invert(testObject)

if (result.length != 0) {
    console.log("invert of our object is :- ", result)
}