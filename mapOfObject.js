const keys = require('./keys')
const values = require('./values')

function mapOfObject(userObject, callback) {

    if (typeof (userObject) === 'object' && (typeof (callback) === 'function')) {

        const keysList = keys(userObject)
        const valueList = values(userObject)

        const size = keysList.length;
        for (let index = 0; index < size; index++) {
            userObject[keysList[index]] = callback(keysList[index], valueList[index])
        }
        return userObject;

    } else {
        console.log("your passed agrument is not object, please try again")
        return {}
    }
}

module.exports = mapOfObject;