function values(userObject) {

    if (typeof (userObject) === 'object') {

        let valueList = []
        for (let key in userObject) {
            valueList.push(userObject[key])
        }

        return valueList

    } else {
        console.log("your passed agrument is not object, please try again")
        return []
    }
}

module.exports = values